﻿


namespace Example.UI
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class Menu
    {
        public Menu()
        {
            try
            {
                PageFactory.InitElements(Driver.driver, this);
            }
            finally
            {

            }
           
        }

        [FindsBy(How = How.Id, Using = "menu-item-25")]
        public IWebElement Introduction { get; set; }

        [FindsBy(How = How.Id, Using = "menu-item-106")]
        public IWebElement Selectors { get; set; }

        [FindsBy(How = How.Id, Using = "menu-item-35")]
        public IWebElement SpecialElements { get; set; }

        [FindsBy(How = How.Id, Using = "menu-item-57")]
        public IWebElement TestCase { get; set; }

        [FindsBy(How = How.Id, Using = "menu-item-28")]
        public IWebElement TestScenarios { get; set; }

        [FindsBy(How = How.Id, Using = "menu-item-26 ")]
        public IWebElement About { get; set; }
    }
}
