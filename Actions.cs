﻿

using Example.UI;

namespace Example
{
  public static  class Actions
    {
        public static void FillLoginForm(string username, string password, string repeatPassord)
        {
            LoginScenarioPost lgpost = new LoginScenarioPost();
            lgpost.UsernameField.SendKeys(username);
            lgpost.PasswordField.SendKeys(password);
            lgpost.RepeatPasswordField.SendKeys(repeatPassord);
            lgpost.LoginButton.Click();
        }
    }
}
