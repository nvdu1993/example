﻿

using Example.UI;
using System.Threading;

namespace Example
{
    public static class NavigateTo
    {
        public static void LoginFormThroughTheMenu()
        {
            Menu menu = new Menu();
            TestScenariosPage tsPage = new TestScenariosPage();
            // menu.TestScenarios.Click();
            menu.TestCase.Click();
            tsPage.LoginFormScenario.Click();
            tsPage.ClickForm.Click();

        }
        public static void LoginFormThroughThePost()
        {
            Menu menu = new Menu();
            TestCasesPage tcPage = new TestCasesPage();
            UserNameFieldPost ufPost = new UserNameFieldPost();

            menu.TestCase.Click();
            Thread.Sleep(500);
            tcPage.UsernameCase.Click();
            Thread.Sleep(500);
            ufPost.LoginFormLink.Click();
            Thread.Sleep(500);

        }
    }
}
